import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Array;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Controller {

    private static int cport;
    private static int r;
    private static int timeout;
    private static int rebalance_period;

    private static ConcurrentHashMap<Integer, Socket> connectionSockets;

    private static ConcurrentHashMap<String, CountDownLatch> removeAcknowledgements;

    private static ConcurrentHashMap<String, CountDownLatch> storeAcknowledgements;

    private static Index index;

    private static ConcurrentHashMap<LoadIdentifier, Integer[]> portsTriedOnLoad;

    private static class LoadIdentifier{
        public Socket socket;
        public String filename;

        public LoadIdentifier(Socket socket, String filename){
            this.socket = socket;
            this.filename = filename;
        }
    }

    private static class Index{
        private ArrayList<String> filesStored;
        private ConcurrentHashMap<String,ArrayList<Integer>> filesStoredWhere;
        private ConcurrentHashMap<String,String> fileEntryInfo;

        private ConcurrentHashMap<String,Integer> fileSize;

        public Index(){
            this.filesStored = new ArrayList<>();
            this.filesStoredWhere = new ConcurrentHashMap<>();
            this.fileEntryInfo = new ConcurrentHashMap<>();
            this.fileSize = new ConcurrentHashMap<>();
            removeAcknowledgements = new ConcurrentHashMap<>();
            storeAcknowledgements = new ConcurrentHashMap<>();
        }

        public void addFile(String filename, Integer[] where, Integer size){
            synchronized (index) {
                filesStored.add(filename);
                filesStoredWhere.put(filename, new ArrayList<Integer>(List.of(where)));
                fileSize.put(filename, size);
            }
        }

        public void removeFile(String filename){
            synchronized (index) {
                filesStored.remove(filename);
                filesStoredWhere.remove(filename);
                fileSize.remove(filename);
                fileEntryInfo.put(filename, "remove complete");
            }
        }

        public ArrayList<String> getFiles(){
            return filesStored;
        }

        public ArrayList<Integer> getWhere(String filename){
            return filesStoredWhere.get(filename);
        }

        public String getFileEntryInfo(String filename){
            return fileEntryInfo.get(filename);
        }

        public void setFileEntryInfo(String filename, String info){
            fileEntryInfo.put(filename, info);
        }

        public Integer getFileSize(String filename){
            return fileSize.get(filename);
        }

    }

    public static void main(String[] args) {
        initiate(args);
        new Thread(() -> listenLoop()).start();
    }

    public static void initiate(String[] args) {
        int[] newArgs = new int[args.length];
        for(int i = 0; i < args.length; i++) {
            newArgs[i] = Integer.parseInt(args[i]);
        }
        cport = newArgs[0]; r = newArgs[1]; timeout = newArgs[2]; rebalance_period = newArgs[3];
        portsTriedOnLoad = new ConcurrentHashMap<>();
        index = new Index();
        connectionSockets = new ConcurrentHashMap<>();
    }

    private static void listenLoop() {
        try{
            ServerSocket ss = new ServerSocket(cport);
            for(;;){
                try{
                    System.out.println("Waiting for message...");
                    Socket client = ss.accept();
                    new Thread(() -> {
                        try {
                            incomingMessage(client);
                        } catch (IOException | InterruptedException e) {
                            throw new RuntimeException(e); //log if error xd
                        }
                    }).start();

                }catch(Exception e){System.out.println("error "+e);}
            }
        }catch(Exception e){System.out.println("error "+e);}
    }

    private static void incomingMessage(Socket client) throws IOException, InterruptedException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(client.getInputStream()));
        String line;
        while((line = in.readLine()) != null) {
            System.out.println(line + " recieved");
            messageRecievedHandler(line, client);
        }
        //check if connection is dstore or client and do the appropriate thing now its dropped
        client.close();
    }

    private static void sendMessage(String message, Socket client) throws IOException {
        PrintWriter out = new PrintWriter(client.getOutputStream(), true);
        out.println(message);
        out.flush();
    }


    /*
     * Controller can accept the following signals:
     * LOAD
     * RELOAD
     * STORE
     * STORE_ACK
     * REMOVE
     * JOIN
     * REMOVE_ACK
     * REBALANCE_COMPLETE
     * LIST
     */
    private static void messageRecievedHandler(String message, Socket client) throws IOException, InterruptedException {
        String[] messageSplit = message.split(" ");
        String signal = messageSplit[0];
        //check if enough dstores are connected, otherwise return
        if(connectionSockets.size() < r){
            if(!signal.equals("JOIN")){
                sendMessage("ERROR_NOT_ENOUGH_DSTORES", client);
                return;
            }
        }
        //gets tail of message split
        String[] args = new String[messageSplit.length-1];
        for(int i=0; i<messageSplit.length-1; i++){
            args[i]=messageSplit[i+1];
        }
        switch(signal) {
            case "LOAD":
                load(args,client);
                break;
            case "RELOAD":
                reload(args,client);
                break;
            case "STORE":
                System.out.println("store recieved 1");
                String filenameS = args[0];
                //check file not already being stored)
                synchronized (index) {
                    if (!index.getFiles().contains(filenameS)) {
                        System.out.println("file not previously added: adding index info field");
                        index.setFileEntryInfo(filenameS, " ");
                    }

                    if (index.getFiles().contains(filenameS) && !Objects.equals(index.getFileEntryInfo(filenameS), " ")) { //on a file being removed it is removed from here
                        System.out.println("file already stored");
                        sendMessage("ERROR_FILE_ALREADY_EXISTS", client);
                        return;
                    } else if (index.getFileEntryInfo(filenameS).equals("store in progress") || index.getFileEntryInfo(filenameS).equals("remove in progress")) {
                        System.out.println("file is being stored or removed currently");
                        sendMessage("ERROR_FILE_ALREADY_EXISTS", client);
                        return;
                    }
                    CountDownLatch storeLatch = new CountDownLatch(r);
                    storeAcknowledgements.put(filenameS, storeLatch);
                    System.out.println("store recieved 2");
                    String fileName = args[0];
                    index.setFileEntryInfo(fileName, "store in progress");
                }
                store(args, client);
                storeAcknowledgements.remove(filenameS);
                break;
            case "STORE_ACK":
                storeAck(args);
                break;
            case "REMOVE":
                System.out.println("remove recieved 1");
                String filenameR = args[0];
                synchronized (index) {
                    if (!index.filesStored.contains(filenameR)) {
                        System.out.println("file does not exist");
                        sendMessage("ERROR_FILE_DOES_NOT_EXIST", client);
                        return;
                    } else if (index.getFileEntryInfo(filenameR) == "store in progress" || index.getFileEntryInfo(filenameR) == "remove in progress") {
                        System.out.println("file is being stored or removed currently");
                        sendMessage("ERROR_FILE_DOES_NOT_EXIST", client);
                        return;
                    }
                    CountDownLatch latch = new CountDownLatch(index.getWhere(filenameR).size());
                    removeAcknowledgements.put(filenameR, latch);
                    System.out.println("remove recieved 2");
                    String fileName = args[0];
                    index.setFileEntryInfo(fileName, "remove in progress");
                }
                removeRecieved(args, client);
                removeAcknowledgements.remove(filenameR);
                break;
            case "JOIN":
                join(args,client);
                break;
            case "REMOVE_ACK":
                removeAck(args);
                break;
            case "REBALANCE_COMPLETE":
                rebalanceComplete(args);
                break;
            case "LIST":
                listRecieved(client);
                break;
            default:
                System.out.println("Invalid signal recieved");
        }
    }

    private static void listRecieved(Socket client) throws IOException {
        System.out.println("list recieved");
        ArrayList<String> files = index.getFiles();
        StringBuilder sb = new StringBuilder();
        sb.append("LIST ");
        for(String file : files){
            sb.append(file);
            sb.append(" ");
        }
        sendMessage(sb.toString(), client);
    }

    private static void rebalanceComplete(String[] args) {
        System.out.println("rebalance complete");
        //TODO: implement rebalance complete later when doing rebalance xd
    }

    private static void removeRecieved(String[] args,Socket client) throws IOException, InterruptedException {
        String fileName = args[0];
        ArrayList<Integer> fileLocations = index.getWhere(fileName); //gets the ports of the Dstores the file is on
        for(int port : fileLocations){
            sendMessage("REMOVE "+fileName, connectionSockets.get(port));//sends the remove signal to the Dstore, sockets stored above
        }
        if(removeAcknowledgements.get(fileName).await(timeout,TimeUnit.MILLISECONDS)){
            System.out.println("remove complete");
            sendMessage("REMOVE_COMPLETE", client);
            index.removeFile(fileName);
        }else{
            System.out.println("remove timed out"); // controller cannot connect to dstore SHOULD(but doesnt yet) return the same thing: look at spec
        }
    }

    private static void removeAck(String[] args) {
        System.out.println("remove acknowledged");
        String fileName = args[0];
        if(removeAcknowledgements.containsKey(fileName)){
            removeAcknowledgements.get(fileName).countDown();
        }else{
            System.out.println("remove ack recieved but no remove in progress");
        }
    }

    private static void join(String[] args,Socket client) throws IOException {
        Integer port = Integer.parseInt(args[0]);
        System.out.println("join recieved");
        if(connectionSockets.containsKey(port)){
            System.out.println("already connected to this dstore");
        }else {
            System.out.println("connecting to a new dstore");
            connectionSockets.put(port, client);
            //todo make a rebalance here
        }
    }

    private static void storeAck(String[] args) {
        System.out.println("store acknowledged");
        String fileName = args[0];
        if(storeAcknowledgements.containsKey(fileName)){
            storeAcknowledgements.get(fileName).countDown();
        }else{
            System.out.println("store ack recieved but no store in progress");
        }
    }

    private static void store(String[] args, Socket client) throws IOException, InterruptedException {
        String fileName = args[0];
        //the below finds the first r dstores that 1) arent storing the file 2)storing the least files
        HashMap<Integer,Integer> dstoreToFileCount = new HashMap<>();
        //filling hashmap with 0s:
        for(int port : connectionSockets.keySet()){
            dstoreToFileCount.put(port,0);
        }
        for(String fileStored: index.filesStored){
            ArrayList<Integer> fileStoredLocations = index.getWhere(fileStored);
            for(int port : fileStoredLocations) {
                dstoreToFileCount.put(port, dstoreToFileCount.get(port) + 1);
            }
        }
        List<Integer> portByFileCount = new ArrayList<>(dstoreToFileCount.keySet());
        Collections.sort(portByFileCount, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return dstoreToFileCount.get(o1) - dstoreToFileCount.get(o2);
            }
        });

        //sends the store signal to the first r dstores
        String message = "STORE_TO ";
        Integer[] ports = new Integer[r];
        for(int i = 0; i < r; i++){
            ports[i] = portByFileCount.get(i);
            message += portByFileCount.get(i) + " ";
        }
        sendMessage(message, client);
        index.setFileEntryInfo(fileName, "store complete");


        if(storeAcknowledgements.get(fileName).await(timeout, TimeUnit.MILLISECONDS)){ //wait until we get all acks back - true if we did, false if timeout
            index.addFile(fileName, ports, Integer.valueOf(args[1])); //args[1] is the filesize
            sendMessage("STORE_COMPLETE", client);
        }else{
            System.out.println("store timed out");
            index.removeFile(fileName);
        }

    }

    private static void reload(String[] args,Socket client) throws IOException {
        System.out.println("reload recieved");
        String fileName = args[0];
        LoadIdentifier loadIdentifier = new LoadIdentifier(client,fileName);
        if(!portsTriedOnLoad.containsKey(loadIdentifier)){
            System.out.println("no load identifier for file and socket combo yet: making one now");
            var temp = new Integer[r];
            for(int i = 0; i < r; i++){
                temp[i] = -1;
            }
            temp[0] = index.getWhere(fileName).get(0);
            portsTriedOnLoad.put(loadIdentifier, temp);
        }
        ArrayList<Integer> portsTriedBefore = new ArrayList<Integer>(List.of(portsTriedOnLoad.get(loadIdentifier))); //initially set to where ports have been tried before
        ArrayList<Integer> ports = index.getWhere(fileName); //initial length = r
        portsTriedBefore.removeAll(Collections.singleton(-1)); //remove all -1s
        ports.removeAll(portsTriedBefore); //size = r-length(portsTriedBefore)

        if(ports.size() == 0){
            System.out.println("all Dstores tried for this file, D:");
            sendMessage("ERROR_LOAD", client);
            return;
        }else{
            System.out.println("trying to load from Dstore " + ports.get(0));
            String message = "LOAD_FROM " + ports.get(0) + " " + index.getFileSize(fileName);
            sendMessage(message, client);

            //add the dstore that has been tried to the portsTriedOnLoad structure
            Integer[] newPortsTried = portsTriedOnLoad.get(loadIdentifier);
            newPortsTried[portsTriedBefore.size()] = ports.get(0); //setting the next value in the array to be the port being tried just now. Wont ever call newPortTried[r] due to if statement
            portsTriedOnLoad.put(loadIdentifier, newPortsTried);
        }

    }

    private static void load(String[] args,Socket client) throws IOException,  InterruptedException {
        System.out.println("load recieved");
        Integer[] ports = new Integer[r];
        String fileName = args[0];

        //check if file is not in index
        if(!index.filesStored.contains(fileName)){
            System.out.println("file not stored");
            sendMessage("ERROR_FILE_DOES_NOT_EXIST", client);
            return;
        }else if(index.getFileEntryInfo(fileName)=="store in progress" || index.getFileEntryInfo(fileName)=="remove in progress"){
            System.out.println("file is being stored or removed currently");
            sendMessage("ERROR_FILE_DOES_NOT_EXIST", client);
            return;
        }

        //find a Dstore that contains the file
        Integer port = index.getWhere(fileName).get(0);
        ports[0] = port;
        //send LOAD_FROM to the Dstore
        String message = "LOAD_FROM " + port + " " + index.getFileSize(fileName);
        sendMessage(message, client);

        //add the dstore that has been tried to the portsTriedOnLoad structure
        LoadIdentifier loadIdentifier = new LoadIdentifier(client,fileName);
        portsTriedOnLoad.put(loadIdentifier,ports);
    }
}


//done :)
