import java.io.*;
import java.net.*;

public class testSender {
    public testSender(String[] args) {

    }

    public static void main(String[] args) throws UnknownHostException {
        //for(int i =0;i<10;i++) {
        //    sendMessage(String.valueOf(i), 5555);
        //}
        sendFile("/home/layla/Desktop/networking/inputfiles/johnTPUSA.jpg", 5555);
    }

    public static int sendMessage(String message, int port) throws UnknownHostException {
        try{
            Socket socket = new Socket(InetAddress.getLocalHost(), port);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(message); out.flush();
            System.out.println("Message sent: " + message);
            socket.close();
            Thread.sleep(1000);
            return 0;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 1;
    }

    public static int sendFile(String fileName, int port) throws UnknownHostException {
        try(Socket socket = new Socket(InetAddress.getLocalHost(), port)){
            File myFile = new File(fileName);
            try (FileInputStream fis = new FileInputStream(myFile)) {
                byte[] mybytearray = fis.readAllBytes();
                OutputStream os = socket.getOutputStream();
                os.write(mybytearray, 0, mybytearray.length);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        return 1;
    }
}


//done :)
