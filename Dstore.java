import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class Dstore {
    private static int port;
    private static int cport;
    private static int timeout;
    private static Path file_folder;

    private static ConcurrentHashMap<String,Integer> filesStored;

    private static Socket controllerSocket;
    private static ServerSocket clientsServerSocket;

    public static void main(String[] args) throws IOException {
        initiate(args);
        new Thread(() -> listenLoopController()).start();
        new Thread(() -> listenLoopClients()).start();
    }

    public static void initiate(String[] args) throws IOException {
        if(args.length != 4){
            System.out.println("Usage: java Dstore <port> <cport> <timeout> <file_folder>");
            return;
        }
        port = Integer.parseInt(args[0]);
        cport = Integer.parseInt(args[1]);
        timeout = Integer.parseInt(args[2]);
        file_folder = Path.of(args[3]);
        filesStored = new ConcurrentHashMap<>();

    }

    private static void listenLoopController(){
        try{
            controllerSocket = new Socket(InetAddress.getLocalHost(), cport);
            if (file_folder.toFile().exists()) {
                for (var file : Objects.requireNonNull(file_folder.toFile().listFiles())) {
                    file.delete();
                }
            }
            file_folder.toFile().mkdirs();

            //initial join
            String message = "JOIN " + port;
            PrintWriter out = new PrintWriter(controllerSocket.getOutputStream(), true);
            out.println(message); out.flush();
            System.out.println("Message sent: " + message);

            //listen for messages from controller
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(controllerSocket.getInputStream()));
            String line;
            while((line = in.readLine()) != null) { //only returns null when connection closed
                System.out.println(line + " recieved");
                messageRecievedControllerHandler(line, controllerSocket);
            }
        } catch (Exception e) {
            System.out.println("error "+e);
        }
    }

    private static void listenLoopClients() {
        try{
            clientsServerSocket = new ServerSocket(port);
            for(;;){
                try{
                    System.out.println("Waiting for Client(s) message...");
                    Socket client = clientsServerSocket.accept();
                    new Thread(() -> {
                        try {
                            incomingMessageClient(client);
                        } catch (IOException | InterruptedException e) {
                            throw new RuntimeException(e); //log if error xd
                        }
                    }).start();

                }catch(Exception e){System.out.println("error "+e);}
            }

        } catch (Exception e) {
            System.out.println("error "+e);
        }

    }

    private static void incomingMessageClient(Socket client) throws IOException, InterruptedException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(client.getInputStream()));
        String line;
        while((line = in.readLine()) != null) { //only returns null when connection closed
            System.out.println(line + " recieved");
            messageRecievedClientHandler(line, client);
        }
    }

    private static void messageRecievedControllerHandler(String line, Socket controllerSocket) throws IOException {
        String[] parts = line.split(" ");
        String commands = parts[0];

        String[] args = new String[parts.length-1];
        for(int i=0; i<parts.length-1; i++){
            args[i]=parts[i+1];
        }

        switch(commands){
            case "REMOVE":
                remove(args);
                break;
            default:
                System.out.println("Unknown command");
        }
    }

    private static void remove(String[] args) throws IOException {
        String fileName = args[0];
        if(!filesStored.containsKey(fileName)){
            System.out.println("File not found");
            sendMessageController("ERROR_FILE_DOES_NOT_EXIST " + fileName);
            return;
        }else{
            File file = new File(file_folder.resolve(fileName).toString());
            file.delete();
            filesStored.remove(fileName);
            sendMessageController("REMOVE_ACK " + fileName);
        }
    }


    private static void messageRecievedClientHandler(String line, Socket client) throws IOException {
        String[] parts = line.split(" ");
        String commands = parts[0];

        String[] args = new String[parts.length-1];
        for(int i=0; i<parts.length-1; i++){
            args[i]=parts[i+1];
        }

        switch(commands){
            case "STORE":
                store(args, client);
                break;
            case "LOAD_DATA":
                loadData(args, client);
                break;
            default:
                System.out.println("Unknown command");
        }
    }

    private static void store(String[] args,Socket client) throws IOException {
        String fileName = args[0];
        Integer fileSize = Integer.parseInt(args[1]);
        sendMessageClient("ACK", client);
        InputStream in = client.getInputStream();
        byte[] byteArrayFileIn = in.readNBytes(fileSize);
        try (FileOutputStream stream = new FileOutputStream(String.valueOf(file_folder.resolve(fileName)))) {
            stream.write(byteArrayFileIn);
        }
        sendMessageController("STORE_ACK " + fileName);
        filesStored.put(fileName, fileSize);
    }

    private static void loadData(String[] args,Socket client) throws IOException {
        String fileName = args[0];
        if(!filesStored.containsKey(fileName)){
            System.out.println("File not found");
            client.close();
            return;
        }else{
            try {
                File file = new File(file_folder.resolve(fileName).toString());
                try(FileInputStream fis = new FileInputStream(file)) {
                    byte[] mybytearray = fis.readAllBytes();
                    OutputStream os = client.getOutputStream();
                    os.write(mybytearray, 0, mybytearray.length);
                }
            }catch (Exception e){
                System.out.println("error: " + e.getMessage());
            }
        }

    }

    private static void sendMessageController(String message) throws IOException {
        PrintWriter out = new PrintWriter(controllerSocket.getOutputStream(), true);
        out.println(message); out.flush();
    }

    private static void sendMessageClient(String message, Socket client) throws IOException {
        PrintWriter out = new PrintWriter(client.getOutputStream(), true);
        out.println(message); out.flush();
    }
}


// done :)
